﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EmployeeApp.Models;
using EmployeeApp.Models.Dto;
using EmployeeApp.Data;

namespace EmployeeApp.Controllers
{
    public class HomeController : Controller
    {
        private ApplicationDbContext _db { get; set; }

        public HomeController()
        {
            _db = new ApplicationDbContext();
        }

        public ActionResult Index()
        {
            var employees = _db.Employees.ToList();

            return View(employees);
        }

        [HttpGet]
        public ActionResult AddEmployee()
        {
            return View();
        }

        [HttpPost]
        public void CreateEmployee(EmployeeDto data)
        {
            var employee = new Employee
            {
                FirstName = data.FirstName,
                LastName = data.LastName,
                BirthDate = DateTime.Parse(data.BirthDate)
            };

            _db.Employees.Add(employee);
            _db.SaveChanges();
        }

        [HttpDelete]
        public void DeleteEmployee(int id)
        {
            var employee = _db.Employees.FirstOrDefault(e => e.Id == id);

            if (employee == null)
            {
                throw new Exception("Invalid Id");
            }

            _db.Employees.Remove(employee);
            _db.SaveChanges();
        }

        [HttpGet]
        public ActionResult EditEmployee(int id)
        {
            var employee = _db.Employees.FirstOrDefault(e => e.Id == id);

            return View(employee);
        }

        [HttpPut]
        public void EditEmployee(int id, EmployeeDto data)
        {
            var employee = _db.Employees.FirstOrDefault(e => e.Id == id);

            if (employee == null)
            {
                throw new Exception("Invalid Id");
            }

            employee.FirstName = data.FirstName;
            employee.LastName = data.LastName;
            employee.BirthDate = DateTime.Parse(data.BirthDate);

            _db.SaveChanges();
        }
    }
}