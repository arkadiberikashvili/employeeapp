﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EmployeeApp.Models.Dto
{
    public class EmployeeDto
    {
        public string FirstName { get; set; }
        public string LastName { get; set; }

        public string BirthDate { get; set; }
    }
}